//module.exports = (() => {

    // set variables for app
    var express     = require('express');
    var app         = express();
    var path        = require('path');
    var bodyParser  = require('body-parser');

    var mysql       = require('mysql');
    var credentials;
    try{
        credentials = require('./credentials'); //CREATE THIS FILE YOURSELF
    }catch(e){
        //heroku support
        credentials = require('./credentials_env');
    }

    // Setup MySQL Connection
    var connection  = mysql.createConnection(credentials);
    // Connect to MySQL DB
    connection.connect();

    // configure app to use bodyParser()
    // this will let us get the data from a POST
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    // views as directory for all template files
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs'); // use either jade or ejs       
    // instruct express to server up static assets
    app.use('/static', express.static('public'));

    // Support for Crossdomain JSONP
    app.set('jsonp callback name', 'callback');

    // Get the Routes for our API
    var apiRouter = require('./routers/api')(express, connection);

    // Apply Routes to App
    // All of these routes will be prefixed with /api
    app.use('/api', apiRouter);

    // non api route for our views
    app.get('/', (req, res) => {
        var query = connection.query('SELECT * FROM ecoinfo ORDER BY id DESC LIMIT 0, 1', (err, rows, fields) => {
            if (err) {
                //INVALID
                console.error(err);
                res.sendStatus(404);
            }
            //Ecoinfo Object
            let ecoinfo = {
                "Date" : rows[0]["Date"],
                "EUR" : rows[0]["EUR"],
                "JPY" : rows[0]["JPY"],
                "USD" : rows[0]["USD"],
                "RUB" : rows[0]["RUB"],
                "Gold" : rows[0]["Gold"].substring(1,0)+rows[0]["Gold"].substring(2),
                "Silver" : rows[0]["Silver"],
                "Platinum" : rows[0]["Platinum"],
                "Copper" : rows[0]["Copper"],
                "Zinc" : rows[0]["Zinc"],
                "Lead" : rows[0]["Lead"],
                "Coal" : rows[0]["Coal"],
                "Oil" : rows[0]["Oil"],
                "Gasonline" : rows[0]["Gasonline"],
                "Diesel" : rows[0]["Diesel"],
                "Midoil" : rows[0]["Midoil"]
            };
            console.log(ecoinfo);
            res.render('index', ecoinfo);
        });
        console.log(query.sql);
    });

    // Better way to disable x-powered-by
    app.disable('x-powered-by');

   /* return app;
})();*/

module.exports = app;