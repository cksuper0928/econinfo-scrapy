const request = require('request');
var util = require('util');
//Dependencies - Express 4.x and the MySQL Connection
module.exports = (express, connection) => {
	var router      = express.Router();

	// Router Middleware
	router.use((req, res, next) => {
	    // log each request to the console
	    console.log("You have hit the /api", req.method, req.url);

	    // Remove powered by header
	    //res.set('X-Powered-By', ''); // OLD WAY
	    //res.removeHeader("X-Powered-By"); // OLD WAY 2
	    // See bottom of script for better way

	    // CORS 
	    res.header("Access-Control-Allow-Origin", "*"); //TODO: potentially switch to white list version
	    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

	    // we can use this later to validate some stuff

	    // continue doing what we were doing and go to the route
	    next();
	});

	// API ROOT - Display Available Routes
	router.get('/', (req, res) => {
		var query = connection.query('SELECT * FROM ecoinfo ORDER BY id DESC LIMIT 0, 1', (err, rows, fields) => {
            if (err) {
                //INVALID
                console.error(err);
                res.sendStatus(404);
            }else{
                if(rows.length){
                    res.jsonp(rows);
                }else{
                    //ID NOT FOUND
                    res.sendStatus(404);
                }
            }
        });
        console.log(query.sql);
	    res.jsonp({
	        name: 'Ecoinfo API', 
	        version: '1.0',
//	        routes: routes // TODO: format this better, after above is fixed
	    });
	});

	// Simple MySQL Test
	router.get('/test', (req, res) => {
	    var test;
	    
	    connection.query('SELECT 1 + 1 AS solution', (err, rows, fields) => {
	        if (err) throw err;

	        test = rows[0].solution;

	        res.jsonp({
	            'test': test
	        });
	    }); 
	});

	// http://www.restapitutorial.com/lessons/httpmethods.html
	// POST - Create
	// GET - Read
	// PUT - Update/Replace - AKA you pass all the data to the update
	// PATCH - Update/Modify - AKA you just pass the changes to the update
	// DELETE - Delete

	// COLLECTION ROUTES
	router.route('/panoramas')
	    //we can use .route to then hook on multiple verbs
	    .post((req, res) => {
	        var data = req.body; // maybe more carefully assemble this data
	        console.log(req.body)
	        var query = connection.query('INSERT INTO ecoinfo SET ?', [data], (err, result) => {
	            if(err){
	                console.error(err);
	                res.sendStatus(404);
	            }else{
	                res.status(201);
	                res.location('/api/panoramas/' + result.insertId);
	                res.end();
	            }
	        });
	        console.log(query.sql);
	    })

	    .get((req, res) => {
	        var query = connection.query('SELECT * FROM ecoinfo', (err, rows, fields) => {
	            if (err) console.error(err);

	            res.jsonp(rows);
	        });
	        console.log(query.sql);
	    })

	    //We do NOT do these to the collection
	    .put((req, res) => {
	        //res.status(404).send("Not Found").end();
	        res.sendStatus(404);
	    })
	    .patch((req, res) => {
	        res.sendStatus(404);
	    })
	    .delete((req, res) => {
	        // LET's TRUNCATE TABLE..... NOT!!!!!
	        res.sendStatus(404);
	    });
	//end route

	// SPECIFIC ITEM ROUTES
	router.route('/panoramas/:id')
	    .post((req, res) => {
	        //specific item should not be posted to (either 404 not found or 409 conflict?)
	        res.sendStatus(404);
	    })

	    .get((req, res) => {
	        var query = connection.query('SELECT * FROM ecoinfo WHERE id=?', [req.params.id], (err, rows, fields) => {
	            if (err) {
	                //INVALID
	                console.error(err);
	                res.sendStatus(404);
	            }else{
	                if(rows.length){
	                    res.jsonp(rows);
	                }else{
	                    //ID NOT FOUND
	                    res.sendStatus(404);
	                }
	            }
	        });
	        console.log(query.sql);
	    })

	    .put((req, res) => {
	        var data = req.body;
	        var query = connection.query('UPDATE ecoinfo SET ? WHERE id=?', [data, req.params.id], (err, result) => {
	            if(err){
	                console.log(err);
	                res.sendStatus(404);
	            }else{
	                res.status(200).jsonp({changedRows:result.changedRows, affectedRows:result.affectedRows}).end();
	            }
	        })
	        console.log(query.sql)
	    })

	    .patch((req, res) => {
	        // Need to decide how much this should differ from .put
	        //in theory (hmm) this should require all the fields to be present to do the update?
	    })

	    .delete((req, res) => {
	        //LIMIT is somewhat redundant, but I use it for extra sanity, and so if I bungle something I only can break one row.
	        var query = connection.query('DELETE FROM ecoinfo WHERE id=? LIMIT 1', [req.params.id], (err, result) => {
	            if(err){
	                console.log(err);
	                res.sendStatus(404);
	            }else{
	                res.status(200).jsonp({affectedRows:result.affectedRows}).end();
	            }
	        });
	        console.log(query.sql)
	    });
	//end route

	//Ecoinfo Object
	let ecoinfo = {
		"Date" : "",
		"EUR" : "",
		"JPY" : "",
		"USD" : "",
		"RUB" : "",
		"Gold" : "",
		"Silver" : "",
		"Copper" : "",
		"Zinc" : "",
		"Lead" : "",
		"Coal" : "",
		"Oil" : "",
		"Gasonline" : "",
		"Diesel" : "",
		"Midoil" : ""
	};
	//GET ECONOMIC INFO
	router.get('/ecoinfo', (req, res) => {
		//Get Oilinfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=oilinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Oil"] = json_oilinfo["items"][0]["Oil"];
		});
		//Get Coalinfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=coalinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Coal"] = json_oilinfo["items"][0]["Coal"];
		});
		//Get Currencyinfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=curinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Date"] = json_oilinfo["items"][0]["Date"];
			ecoinfo["EUR"] = json_oilinfo["items"][0]["EUR"];
			ecoinfo["JPY"] = json_oilinfo["items"][0]["JPY"];
			ecoinfo["USD"] = json_oilinfo["items"][0]["USD"];
			ecoinfo["RUB"] = json_oilinfo["items"][0]["RUB"];
		});
		//Get Dieselinfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=dieselinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Diesel"] = json_oilinfo["items"][0]["Diesel"];
		});
		//Get Gasonlinelinfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=gasoinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Gasonline"] = json_oilinfo["items"][0]["Gasonline"];
		});
		//Get Gold,Silver linfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=goldinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Gold"] = json_oilinfo["items"][0]["Gold"];
			ecoinfo["Silver"] = json_oilinfo["items"][0]["Silver"];
			ecoinfo["Platinum"] = json_oilinfo["items"][0]["Platinum"];
		});
		//Get Metal linfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=metalinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Copper"] = json_oilinfo["items"][0]["Copper"];
			ecoinfo["Zinc"] = json_oilinfo["items"][0]["Zinc"];
			ecoinfo["Lead"] = json_oilinfo["items"][0]["Lead"];
		});
		//Get Midoil linfo
		request('http://localhost:8079/crawl.json?start_requests=true&spider_name=midoilinfo', function (error, response, body) {
			json_oilinfo = JSON.parse(body);
			ecoinfo["Midoil"] = json_oilinfo["items"][0]["Midoil"];
		});
		function insert_info (arg) {
			var query = connection.query('INSERT INTO ecoinfo SET ?', [arg], (err, result) => {
	            if(err){
	                console.error(err);
	                res.sendStatus(404);
	            }else{
	                res.status(201);
	                res.location('/api/panoramas/' + result.insertId);
	                res.end();
	            }
	        });
	        console.log(query.sql);
		}
		setTimeout(insert_info, 5000, ecoinfo);
		
		return ecoinfo;
	});

	return router;
};