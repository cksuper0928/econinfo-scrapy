import scrapy


class CurinfoSpider(scrapy.Spider):
    name = "curinfo"

    def start_requests(self):
        urls = [
            'http://www.boc.cn/sourcedb/whpj/enindex.html'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #currency info
        date = response.xpath("//td[contains(text(), 'EUR')]/following-sibling::td[6]/text()")
        eurinfo = response.xpath("//td[contains(text(), 'EUR')]/following-sibling::td[5]/text()")
        usdinfo = response.xpath("//td[contains(text(), 'USD')]/following-sibling::td[5]/text()")
        jpyinfo = response.xpath("//td[contains(text(), 'JPY')]/following-sibling::td[5]/text()")
        rubinfo = response.xpath("//td[contains(text(), 'RUB')]/following-sibling::td[5]/text()")
        yield {
            "Date" : date.get()[0:10] + date.get()[14:23],
            "EUR" : eurinfo.get(),
            "JPY" : jpyinfo.get(),
            "USD" : usdinfo.get(),
            "RUB" : rubinfo.get()
        }

