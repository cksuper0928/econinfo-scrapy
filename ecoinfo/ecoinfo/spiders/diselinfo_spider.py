import scrapy
import json

class DieselinfoSpider(scrapy.Spider):
    name = "dieselinfo"

    def start_requests(self):
        #https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/4655/G?quoteCodes

        url = 'https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/4655/G?quoteCodes'

        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        jsonresponse = json.loads(response.body_as_unicode())

        yield {
            "Diesel" : jsonresponse["quotes"][0]["priorSettle"]
        }

