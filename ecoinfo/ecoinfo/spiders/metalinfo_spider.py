import scrapy


class MetalinfoSpider(scrapy.Spider):
    name = "metalinfo"

    def start_requests(self):
        urls = [
            'http://www.kitcometals.com/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Metal info
        copperinfo = response.xpath('//td[contains(text(), "Copper")]/../../following-sibling::table[1]/tr[1]/td[2]/text()')
        zincinfo = response.xpath('//td[contains(text(), "Zinc")]/../../following-sibling::table[1]/tr[1]/td[2]/text()')
        leadinfo = response.xpath('//td[contains(text(), "Lead")]/../../following-sibling::table[1]/tr[1]/td[2]/text()')
        yield {
            "Copper" : copperinfo.get(),
            "Zinc" : zincinfo.get(),
            "Lead" : leadinfo.get()
        }

