import scrapy


class MidoilinfoSpider(scrapy.Spider):
    name = "midoilinfo"

    def start_requests(self):
        urls = [
            'https://www.oilmonster.com/bunker-fuel-prices/st-petersburg-ifo-180-price/5/135'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Midoil info
        midoilinfo = response.xpath('//div[@class="scrapitemprice"]/text()[2]')
        yield {
            "Midoil" : midoilinfo.get()
        }

