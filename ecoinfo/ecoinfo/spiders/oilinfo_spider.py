import scrapy

class OilinfoSpider(scrapy.Spider):
    name = "oilinfo"

    def start_requests(self):
        url = 'https://finance.naver.com/marketindex/?tabSel=gold#tab_section'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Oil info
        oilinfo = response.xpath('//a[contains(text(), "두바이유")]/../following-sibling::td[2]/text()') 
        yield {
            "Oil" : oilinfo.get()
        }

