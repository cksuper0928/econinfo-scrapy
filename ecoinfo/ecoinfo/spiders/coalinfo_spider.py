import scrapy


class CoalinfoSpider(scrapy.Spider):
    name = "coalinfo"

    def start_requests(self):
        urls = [
            'https://markets.businessinsider.com/commodities/coal-price'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Coal info
        coalinfo = response.xpath('//table[@class="table snapshot-headline-table"]/tr/td[2]/span[1]/div/span/text()')
        yield {
            "Coal" : coalinfo.get()
        }

