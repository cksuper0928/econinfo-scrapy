import scrapy


class GasoinfoSpider(scrapy.Spider):
    name = "gasoinfo"

    def start_requests(self):
        urls = [
            'https://www.globalpetrolprices.com/Russia/gasoline_prices/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Gasonline info
        gasoinfo = response.xpath('//th[contains(text(), "USD")]/following-sibling::td[1]/text()')
        yield {
            "Gasonline" : gasoinfo.get()
        }

