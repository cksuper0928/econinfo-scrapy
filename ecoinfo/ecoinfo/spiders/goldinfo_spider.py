import scrapy


class GoldinfoSpider(scrapy.Spider):
    name = "goldinfo"

    def start_requests(self):
        urls = [
            'http://www.samsunggold.co.kr'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        #Gold, silver info
        goldinfo = response.xpath('//ul[@class="pricing-table2 blue"]/li[@class="detail"]/span[@class="first-line"]/div[@id="stock1"]/text()')
        silverinfo = response.xpath('//ul[@class="pricing-table2 blue"]/li[@class="detail"]/span[@class="first-line"]/div[@id="stock2"]/text()')
        ptinfo = response.xpath('//ul[@class="pricing-table2 blue"]/li[@class="detail"]/span[@class="first-line"]/div[@id="stock3"]/text()')
        yield {
            "Gold" : goldinfo.get(),
            "Silver" : silverinfo.get(),
            "Platinum" : ptinfo.get()
        }

